object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 256
  ClientWidth = 279
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 8
    Top = 8
    Width = 266
    Height = 21
    MaxLength = 20
    TabOrder = 0
    OnChange = Edit1Change
    OnKeyPress = Edit1KeyPress
  end
  object SevenBtn: TBitBtn
    Left = 85
    Top = 67
    Width = 33
    Height = 25
    Caption = '7'
    TabOrder = 1
    OnClick = SevenBtnClick
  end
  object EightBtn: TBitBtn
    Left = 124
    Top = 67
    Width = 33
    Height = 25
    Caption = '8'
    TabOrder = 2
    OnClick = EightBtnClick
  end
  object NineBtn: TBitBtn
    Left = 163
    Top = 67
    Width = 33
    Height = 25
    Caption = '9'
    TabOrder = 3
    OnClick = NineBtnClick
  end
  object FourBtn: TBitBtn
    Left = 85
    Top = 98
    Width = 33
    Height = 25
    Caption = '4'
    TabOrder = 4
    OnClick = FourBtnClick
  end
  object FiveBtn: TBitBtn
    Left = 124
    Top = 98
    Width = 33
    Height = 25
    Caption = '5'
    TabOrder = 5
    OnClick = FiveBtnClick
  end
  object SixBtn: TBitBtn
    Left = 163
    Top = 98
    Width = 33
    Height = 25
    Caption = '6'
    TabOrder = 6
    OnClick = SixBtnClick
  end
  object OneBtn: TBitBtn
    Left = 85
    Top = 129
    Width = 33
    Height = 25
    Caption = '1'
    TabOrder = 7
    OnClick = OneBtnClick
  end
  object TwoBtn: TBitBtn
    Left = 124
    Top = 129
    Width = 33
    Height = 25
    Caption = '2'
    TabOrder = 8
    OnClick = TwoBtnClick
  end
  object ThreeBtn: TBitBtn
    Left = 163
    Top = 129
    Width = 33
    Height = 25
    Caption = '3'
    TabOrder = 9
    OnClick = ThreeBtnClick
  end
  object DivisionBtn: TBitBtn
    Tag = 4
    Left = 202
    Top = 67
    Width = 33
    Height = 25
    Caption = '/'
    TabOrder = 10
    OnClick = PlusBtnClick
  end
  object BitBtn11: TBitBtn
    Left = 241
    Top = 36
    Width = 33
    Height = 25
    Caption = #8730
    TabOrder = 11
    OnClick = BitBtn11Click
  end
  object MultiplyBtn: TBitBtn
    Tag = 3
    Left = 202
    Top = 98
    Width = 33
    Height = 25
    Caption = '*'
    TabOrder = 12
    OnClick = PlusBtnClick
  end
  object BitBtn13: TBitBtn
    Left = 241
    Top = 98
    Width = 33
    Height = 25
    Caption = '1/x'
    TabOrder = 13
    OnClick = BitBtn13Click
  end
  object MinusBtn: TBitBtn
    Tag = 2
    Left = 202
    Top = 129
    Width = 33
    Height = 25
    Caption = '-'
    TabOrder = 14
    OnClick = PlusBtnClick
  end
  object EqualBtn: TBitBtn
    Left = 241
    Top = 129
    Width = 33
    Height = 56
    Caption = '='
    TabOrder = 15
    OnClick = EqualBtnClick
  end
  object ZeroBtn: TBitBtn
    Left = 85
    Top = 160
    Width = 72
    Height = 25
    Caption = '0'
    TabOrder = 16
    OnClick = ZeroBtnClick
  end
  object CommaBtn: TBitBtn
    Left = 163
    Top = 160
    Width = 33
    Height = 25
    Caption = ','
    TabOrder = 17
    OnClick = CommaBtnClick
  end
  object PlusBtn: TBitBtn
    Tag = 1
    Left = 202
    Top = 160
    Width = 33
    Height = 25
    Caption = '+'
    TabOrder = 18
    OnClick = PlusBtnClick
  end
  object BitBtn19: TBitBtn
    Left = 124
    Top = 36
    Width = 33
    Height = 25
    Caption = '<'#8212
    TabOrder = 19
    OnClick = BitBtn19Click
  end
  object ClearBtn: TBitBtn
    Left = 163
    Top = 36
    Width = 33
    Height = 25
    Caption = 'C'
    TabOrder = 20
    OnClick = ClearBtnClick
  end
  object ChangeOperatorBtn: TBitBtn
    Left = 202
    Top = 36
    Width = 33
    Height = 25
    Caption = #177
    TabOrder = 21
    OnClick = ChangeOperatorBtnClick
  end
  object BitBtn23: TBitBtn
    Left = 241
    Top = 67
    Width = 33
    Height = 25
    Caption = '%'
    Enabled = False
    TabOrder = 22
  end
  object CubeBtn: TBitBtn
    Left = 47
    Top = 97
    Width = 33
    Height = 25
    Caption = 'x'#179
    TabOrder = 23
    OnClick = CubeBtnClick
  end
  object SquareBtn: TBitBtn
    Left = 47
    Top = 66
    Width = 33
    Height = 25
    Caption = 'x'#178
    TabOrder = 24
    OnClick = SquareBtnClick
  end
  object FactorialBtn: TBitBtn
    Left = 8
    Top = 159
    Width = 33
    Height = 25
    Caption = 'n!'
    TabOrder = 25
    OnClick = FactorialBtnClick
  end
  object BitBtn3: TBitBtn
    Tag = 5
    Left = 46
    Top = 159
    Width = 33
    Height = 25
    Caption = 'y'#8730'x'
    Enabled = False
    TabOrder = 26
  end
  object lnBtn: TBitBtn
    Left = 8
    Top = 35
    Width = 33
    Height = 25
    Caption = 'ln'
    TabOrder = 27
    OnClick = lnBtnClick
  end
  object sinBtn: TBitBtn
    Left = 8
    Top = 66
    Width = 33
    Height = 25
    Caption = 'sin'
    TabOrder = 28
    OnClick = sinBtnClick
  end
  object tanBtn: TBitBtn
    Left = 8
    Top = 128
    Width = 33
    Height = 25
    Caption = 'tan'
    TabOrder = 29
    OnClick = tanBtnClick
  end
  object cosBtn: TBitBtn
    Left = 8
    Top = 97
    Width = 33
    Height = 25
    Caption = 'cos'
    TabOrder = 30
    OnClick = cosBtnClick
  end
  object PiBtn: TBitBtn
    Left = 47
    Top = 35
    Width = 33
    Height = 25
    Caption = #960
    TabOrder = 31
    OnClick = PiBtnClick
  end
  object powBtn: TBitBtn
    Left = 47
    Top = 129
    Width = 33
    Height = 25
    Caption = 'x^y'
    TabOrder = 32
    OnClick = powBtnClick
  end
  object MainMenu1: TMainMenu
    Left = 119
    Top = 200
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object N2: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        OnClick = N2Click
      end
    end
    object N3: TMenuItem
      Caption = #1055#1088#1072#1074#1082#1072
      object N5: TMenuItem
        Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = #1042#1089#1090#1072#1074#1080#1090#1100
        OnClick = N6Click
      end
    end
    object N4: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1082#1072
    end
  end
end
