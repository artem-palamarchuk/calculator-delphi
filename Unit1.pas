unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.Menus, Math;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    MainMenu1: TMainMenu;
    SevenBtn: TBitBtn;
    EightBtn: TBitBtn;
    NineBtn: TBitBtn;
    FourBtn: TBitBtn;
    FiveBtn: TBitBtn;
    SixBtn: TBitBtn;
    OneBtn: TBitBtn;
    TwoBtn: TBitBtn;
    ThreeBtn: TBitBtn;
    DivisionBtn: TBitBtn;
    BitBtn11: TBitBtn;
    MultiplyBtn: TBitBtn;
    BitBtn13: TBitBtn;
    MinusBtn: TBitBtn;
    EqualBtn: TBitBtn;
    ZeroBtn: TBitBtn;
    CommaBtn: TBitBtn;
    PlusBtn: TBitBtn;
    BitBtn19: TBitBtn;
    ClearBtn: TBitBtn;
    ChangeOperatorBtn: TBitBtn;
    BitBtn23: TBitBtn;
    CubeBtn: TBitBtn;
    SquareBtn: TBitBtn;
    FactorialBtn: TBitBtn;
    BitBtn3: TBitBtn;
    lnBtn: TBitBtn;
    sinBtn: TBitBtn;
    tanBtn: TBitBtn;
    cosBtn: TBitBtn;
    PiBtn: TBitBtn;
    powBtn: TBitBtn;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    procedure OneBtnClick(Sender: TObject);
    procedure TwoBtnClick(Sender: TObject);
    procedure PlusBtnClick(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1Change(Sender: TObject);
    procedure ThreeBtnClick(Sender: TObject);
    procedure FourBtnClick(Sender: TObject);
    procedure FiveBtnClick(Sender: TObject);
    procedure SixBtnClick(Sender: TObject);
    procedure SevenBtnClick(Sender: TObject);
    procedure EightBtnClick(Sender: TObject);
    procedure NineBtnClick(Sender: TObject);
    procedure ZeroBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EqualBtnClick(Sender: TObject);
    procedure ClearBtnClick(Sender: TObject);
    procedure ChangeOperatorBtnClick(Sender: TObject);
    procedure CubeBtnClick(Sender: TObject);
    procedure SquareBtnClick(Sender: TObject);
    procedure FactorialBtnClick(Sender: TObject);
    procedure PiBtnClick(Sender: TObject);
    procedure cosBtnClick(Sender: TObject);
    procedure sinBtnClick(Sender: TObject);
    procedure tanBtnClick(Sender: TObject);
    procedure powBtnClick(Sender: TObject);
    procedure lnBtnClick(Sender: TObject);
    procedure BitBtn19Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure CommaBtnClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  operation: char;
  num1,num2,result,prom: extended;
implementation

{$R *.dfm}

procedure TForm1.FormShow(Sender: TObject);
begin
  Edit1.SetFocus;
end;

procedure TForm1.Edit1Change(Sender: TObject);
 var
 i,n:integer;
begin
  HideCaret(Edit1.Handle);
  n:=pos(',',Edit1.Text);
  for i:=n+1 to length(Edit1.Text) do
    if Edit1.Text[i]=',' then Edit1.Clear;
  if Edit1.Text='00' then Edit1.Text:='0';
end;

procedure TForm1.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  Edit1.SetFocus;
  case key of
    '0'..'9',',',#8:;
    else key:=#0;
  end;
end;

procedure TForm1.PlusBtnClick(Sender: TObject);
begin
  if Edit1.Text <> '' then num1:=StrToFloat(Edit1.Text);
  case (Sender as TBitBtn).Tag of
    1: operation:='+';
    2: operation:='-';
    3: operation:='*';
    4: operation:='/';
  end;
  Edit1.Clear;
end;

procedure TForm1.powBtnClick(Sender: TObject);
begin
 if Edit1.Text<>'' then
 Edit1.Text:=FloatToStr(exp(StrToFloat(Edit1.Text)));
end;

procedure TForm1.EqualBtnClick(Sender: TObject);
begin
  if Edit1.Text<> '' then num2:=StrToFloat(Edit1.Text);
 case operation of
  '+': result:= num1+num2;
  '-': result:= num1-num2;
  '*': result:= num1*num2;
  '/': if num2=0 then showmessage('�� ���� ������ ������')
  else result:= num1/num2;
 end;

 Edit1.Text:=FloatToStr(result);
 num1:=0; num2:=0; operation:=' ';
end;

procedure TForm1.OneBtnClick(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'1';
end;

procedure TForm1.TwoBtnClick(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+'2';
end;

procedure TForm1.ThreeBtnClick(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+'3';
end;

procedure TForm1.FourBtnClick(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+'4';
end;

procedure TForm1.FiveBtnClick(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+'5';
end;

procedure TForm1.SixBtnClick(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+'6';
end;

procedure TForm1.SevenBtnClick(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+'7';
end;

procedure TForm1.EightBtnClick(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+'8';
end;

procedure TForm1.N2Click(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.N5Click(Sender: TObject);
begin
  Edit1.CopyToClipboard;
end;

procedure TForm1.N6Click(Sender: TObject);
begin
  Edit1.PasteFromClipboard;
end;

procedure TForm1.NineBtnClick(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+'9';
end;

procedure TForm1.ZeroBtnClick(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+'0';
end;

procedure TForm1.CommaBtnClick(Sender: TObject);
begin
  Edit1.Text:=Edit1.Text+',';
end;

procedure TForm1.BitBtn19Click(Sender: TObject);
var s:string;
begin
  s:=Edit1.Text;
  delete(s,length(s),1);
  Edit1.Text:=s;
end;

procedure TForm1.ChangeOperatorBtnClick(Sender: TObject);
begin
  Edit1.Text:=FloatToStr(-(StrToFloat(Edit1.Text)));
end;

procedure TForm1.SquareBtnClick(Sender: TObject);
begin
  if Edit1.Text<>'' then
   Edit1.Text:=FloatToStr(StrToFloat(Edit1.Text)*StrToFloat(Edit1.Text));
end;

procedure TForm1.CubeBtnClick(Sender: TObject);
begin
  if Edit1.Text<>'' then
  Edit1.Text:=FloatToStr(StrToFloat(Edit1.Text)*StrToFloat(Edit1.Text)*StrToFloat(Edit1.Text));
end;

procedure TForm1.FactorialBtnClick(Sender: TObject);
var
  x:extended;
  i:integer;
begin
  if Edit1.Text<>'' then
    begin
      x:=StrToFloat(Edit1.Text);
      for i:=1 to round(x)-1 do
      x:=x*i;
      Edit1.Text:=FloatToStr(x);
    end;
end;

procedure TForm1.sinBtnClick(Sender: TObject);
begin
  Edit1.Text:=FloatToStr(sin((StrToFloat(Edit1.Text))));
end;

procedure TForm1.cosBtnClick(Sender: TObject);
begin
  Edit1.Text:=FloatToStr(cos(StrToFloat(Edit1.Text)));
end;

procedure TForm1.tanBtnClick(Sender: TObject);
begin
  Edit1.Text:=FloatToStr(sin(StrToFloat(Edit1.Text))/cos(StrToFloat(Edit1.Text)));
end;

procedure TForm1.lnBtnClick(Sender: TObject);
begin
  if Edit1.Text<>'' then
  if StrToFloat(Edit1.Text)>0 then
    Edit1.Text:=FloatToStr(ln(StrToFloat(Edit1.Text)));
end;

procedure TForm1.BitBtn11Click(Sender: TObject);
begin
  Edit1.Text:=FloatToStr(sqrt(StrToFloat(Edit1.Text)));
end;

procedure TForm1.BitBtn13Click(Sender: TObject);
begin
  if Edit1.Text<>'' then
  if StrToFloat(Edit1.Text)>0 then
    Edit1.Text:=FloatToStr(1/(StrToFloat(Edit1.Text)));
end;

procedure TForm1.PiBtnClick(Sender: TObject);
begin
  Edit1.Text:=FloatToStr(pi);
end;

procedure TForm1.ClearBtnClick(Sender: TObject);
begin
  Edit1.Clear;
end;

end.
